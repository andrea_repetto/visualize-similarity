import argparse
from SimilarityTableGenerator import build_and_visualize

# Predefined names for Hampson dataset
fileNames = []
thumbnails = []
for i in xrange(395):
    fileNames.append("{0}.off".format(i+1))
    thumbnails.append("hampson/thumbnails/{0}.png".format(i+1))

def main():

    parser = argparse.ArgumentParser(description="Builds a visualization of a similarity measure")
    parser.add_argument('similarity_matrix', help="The similarity matrix")

    args = parser.parse_args()

    build_and_visualize(fileNames, thumbnails, args.similarity_matrix, "build/hampson")

if __name__ == "__main__":
    main()
