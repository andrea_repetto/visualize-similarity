function matrix=analisi_recuperi(folder,matrix)

indici = [1:395];

[m,i]=sort(matrix);
elem=size(indici,2);
numero_recuperi=20;

for j=1:elem
  vettore=i(1:numero_recuperi,j)';
  distanze=m(1:numero_recuperi,j)';
  l=RecuperiHTML(folder,indici,vettore,distanze,j);
end
end
