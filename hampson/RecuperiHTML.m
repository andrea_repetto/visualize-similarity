function [l]=RecuperiHTML(folder,indici,vettore,distanze,ll)
l=size(vettore,2);

filename=sprintf('%s/%d.html',folder,ll)
fid = fopen(filename,'wt');
fprintf(fid,'%s\n','<HTML> <HEAD> <TITLE>');
fprintf(fid,'%s %d\n','Recupero modello ',ll);
fprintf(fid,'%s\n %s\n','</TITLE> </HEAD>','<BODY>');
fprintf(fid,'%s %d %s\n','<H3> Matching modello ',ll,' </H3>');

for j=1:l
  k=vettore(j);
  name=sprintf('%d',indici(k));
  name1=sprintf('%d.png',indici(k));
  %fprintf(fid,'%s%s%f%s\n%s\n%s\n',name,'dist=',distanze(j),' <BR> <IMG src="',name1,'" width=278/> </BR>');
  fprintf(fid,'%d. %s%s%s %s%s%f%s\n%s\n%s\n',j,'<a href="',name,'.html">',name1,'</a> dist=',distanze(j),' <BR></BR> <IMG src="',name1,'" width=278/> </BR></BR></BR></BR>');
end
fprintf(fid,'%s\n','</BODY> </HTML>');
fclose(fid);
end
