import argparse
from SimilarityTableGenerator import build_and_visualize
import os

# Configuration
base_directory = "gravitate"
image_directory = "IMAGES"
names_file = "originali.txt"
modelname_suffix = "_Im_Al_ActualSize_clean.ply"
thumbnail_suffix = "_thum"
thumb_extensions = ['.jpg', '.png']

def get_filenames():
    names = []
    filePath = os.path.join(base_directory, names_file)
    with open(filePath) as infile:
        names = [line.strip() for line in infile]

    print( "%s items" % len(names) )
    return names

def find_thumbnail(base_name):
    for ext in thumb_extensions:
        file_name = base_name + thumbnail_suffix + ext
        if(os.path.exists(file_name)):
            return file_name
    return None


def get_thumbnails(fileNames, matrix_size = 0):
    thumbnail_names = []
    base_path = os.path.join(base_directory, image_directory)
    for name in fileNames:
        base_name = remove_suffix(name, modelname_suffix)
        full_basename = os.path.join(base_directory, image_directory, base_name)
        thumb = find_thumbnail(full_basename)

        if not thumb:
            print("WARNING: No thumbnail found for artefact '" + base_name + "'")
            continue

        thumbnail_names.append(thumb)

    print( '%s found' % len(thumbnail_names) )
    return thumbnail_names

def remove_suffix(text, suffix):
    if text.endswith(suffix):
        return text[:len(text)-len(suffix)]
    return text

def main():

    parser = argparse.ArgumentParser(description="Builds a visualization of a similarity measure")
    parser.add_argument('similarity_matrix', help="The similarity matrix")

    args = parser.parse_args()

    fileNames = get_filenames()
    thumbnails = get_thumbnails(fileNames)

    build_and_visualize(fileNames, thumbnails, args.similarity_matrix, "build/gravitate")

if __name__ == "__main__":
    main()
