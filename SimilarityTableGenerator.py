import numpy as np
import os
import sys
from shutil import copy2
import webbrowser
import argparse

class SimilarityTableGenerator:

    def __init__(self,
        matrix,
        names_list,
        thumbnails_list,
        thumb_prefix = "thumbnails",
        list_size = 20
        ):
        self.matrix = matrix
        self.names_list = names_list
        self.thumbnails_list = thumbnails_list
        self.thumb_prefix = thumb_prefix
        self.list_size = list_size

        self.html = ""
        self.out_dir = None



    def set_out_dir(self, out_dir):
        self.out_dir = out_dir

    def make_thumb_dir(self):
        path = os.path.join(self.out_dir, self.thumb_prefix)
        if not os.path.exists(path):
            os.makedirs(path)

    def build_index(self):
        out_dir = self.out_dir
        html = """
        <html>
        <body>
        <h1>Shape Similarity</h1>
        <p>Select a shape:</p>
        <table>
        """

        for idx, (name, thumbnail) in enumerate(zip(self.names_list, self.thumbnails_list)):

            filename, ext = os.path.splitext(thumbnail)

            out_thumb_name = self.get_thumbnail_name(idx)

            copy2(thumbnail, os.path.join(out_dir, out_thumb_name))

            page_name = self.get_page_filename(idx)

            html += self.get_index_element(idx)
        html += "</table></body></html>"

        f = open(os.path.join(out_dir, "index.html"), "w")
        f.write(html)
        f.close()

    def build_pages(self):
        for i in xrange(self.matrix.shape[1]):
            self.build_page(i)
        pass

    def build_page(self, page_index):
        page_name = self.get_page_filename(page_index)
        object_name = self.names_list[page_index]

        sorted_indices = np.argsort(self.matrix[:,page_index])[1:self.list_size+1]
        this_thumb = self.get_thumbnail_name(page_index)

        # create sorted arrays
        sorted_dist = self.matrix[sorted_indices, page_index]
        content = ""

        for rank, (idx, dist) in enumerate(zip(sorted_indices, sorted_dist)):
            content += self.get_similarity_element(rank+1, idx, dist)

        html = """
        <html>
        <head>
        <style>
        table, th, td {{
            border: 1px solid black;
        }}
        table {{
            width: 720px;
            margin-left: auto;
            margin-right: auto;
        }}
        h1, h2 {{
            text-align: center
        }}
        </style>
        </head>
        <body>
        <h1>{}</h1>
        <div style="text-align: center; width=720px">
            <img style="height: 300px; width: auto" src="{}" />
        </div>
        <h2 style="text-align center">Similar shapes:</h2>
            <table>
            {}
            </table>
        </body></html>
        """.format(object_name, this_thumb, content)

        out_dir = self.out_dir
        f = open(os.path.join(out_dir, page_name), "w")
        f.write(html)
        f.close()

    def get_thumbnail_name(self, index):
        filename, ext = os.path.splitext(self.thumbnails_list[index])
        return os.path.join(self.thumb_prefix, "thumb_{0:0>4}{1}".format(index, ext))

    def get_page_filename(self, index):
        return "item_{0:0>4}.html".format(index)

    def get_index_element(self, index):
        name = self.names_list[index]
        page = self.get_page_filename(index)
        thumbnail = self.get_thumbnail_name(index)

        html = """<tr>
            <td>{0}</td>
            <td style="text-align:center">
                <a href="{1}">
                    <img style="width: auto; height: 200px" src="{2}" />
                </a>
            </td>
        </tr>
        """
        return html.format(name, page, thumbnail)

    def get_similarity_element(self, rank, index, distance):
        name = self.names_list[index]
        page = self.get_page_filename(index)
        thumbnail = self.get_thumbnail_name(index)

        html = """<tr>
            <td>{}</td>
            <td>{}</td>
            <td style="text-align:center">
                <a href="{}">
                    <img style="width: auto; height: 200px" src="{}" />
                </a>
            </td>
            <td>dist = {}</td>
        </tr>
        """
        return html.format(rank, name, page, thumbnail, distance)

    def build(self):
        self.make_thumb_dir()
        self.build_pages()
        self.build_index()

def read_similarity_matrix_txt(filename):
    """Reads a similarity matrix in text format"""
    print("Loading similarity matrix...")
    matrix = np.loadtxt(filename)
    print("Size: {0}x{1}".format(*matrix.shape))
    return matrix

def build_and_visualize(fileNames, thumbnails, similarity_matrix_txt, out_dir):
    try:
        similarity_matrix = read_similarity_matrix_txt(similarity_matrix_txt)
    except IOError as e:
        print("I/O Error: ({0}): {1} - {2}".format(e.errno, e.filename, e.strerror))
        exit()
    else:
        gen = SimilarityTableGenerator(
            names_list = fileNames,
            thumbnails_list = thumbnails,
            matrix = similarity_matrix
        )

        gen.set_out_dir(out_dir)
        gen.build()

        webbrowser.open(os.path.join(out_dir, "index.html"))
